from django.contrib import admin

from api_v1.models import MachineProductType, PodProductType, Flavor, ProductModel, CoffeeMachine, CoffeePod

admin.site.register(MachineProductType)
admin.site.register(PodProductType)
admin.site.register(Flavor)
admin.site.register(ProductModel)
admin.site.register(CoffeeMachine)
admin.site.register(CoffeePod)