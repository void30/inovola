from rest_framework import serializers

from api_v1.models import CoffeeMachine, CoffeePod, MachineProductType, ProductModel, Flavor, PodProductType


class MachineProductTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = MachineProductType
		fields = '__all__'


class PodProductTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = PodProductType
		fields = '__all__'


class ProductModelSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProductModel
		fields = '__all__'


class FlavorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Flavor
		fields = '__all__'


class CoffeeMachineSerializer(serializers.ModelSerializer):
	class Meta:
		model = CoffeeMachine
		fields = '__all__'

	def to_representation(self, instance):
		response = super().to_representation(instance)
		response['product_type'] = MachineProductTypeSerializer(instance.product_type).data
		response['product_model'] = ProductModelSerializer(instance.product_model).data
		return response


class CoffeePodSerializer(serializers.ModelSerializer):
	class Meta:
		model = CoffeePod
		fields = '__all__'

	def to_representation(self, instance):
		response = super().to_representation(instance)
		response['product_type'] = PodProductTypeSerializer(instance.product_type).data
		response['flavor'] = FlavorSerializer(instance.flavor).data
		return response
