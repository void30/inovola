from django.urls import path

from api_v1.views import CoffeeMachines, CoffeeMachineDetails, CoffeePods, CoffeePodDetails

urlpatterns = [
	path('machines', CoffeeMachines.as_view()),
	path('machines/<int:pk>', CoffeeMachineDetails.as_view()),

	path('pods', CoffeePods.as_view()),
	path('pods/<int:pk>', CoffeePodDetails.as_view()),
]
