from djongo import models


class MachineProductType(models.Model):
	value = models.CharField(max_length=100)

	def __str__(self):
		return self.value


class PodProductType(models.Model):
	value = models.CharField(max_length=100)

	def __str__(self):
		return self.value


class Flavor(models.Model):
	value = models.CharField(max_length=100)

	def __str__(self):
		return self.value


class ProductModel(models.Model):
	value = models.CharField(max_length=100)

	def __str__(self):
		return self.value


class Product(models.Model):
	sku = models.CharField(max_length=50)

	def __str__(self):
		return self.sku

	class Meta:
		abstract = True


class CoffeeMachine(Product):
	product_type = models.ForeignKey(MachineProductType, on_delete=models.DO_NOTHING)
	product_model = models.ForeignKey(ProductModel, on_delete=models.DO_NOTHING)
	water_line_compatible = models.BooleanField()


class CoffeePod(Product):
	product_type = models.ForeignKey(PodProductType, on_delete=models.DO_NOTHING)
	pack_size = models.IntegerField()
	flavor = models.ForeignKey(Flavor, on_delete=models.DO_NOTHING)
