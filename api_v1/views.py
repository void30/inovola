from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics

from api_v1.models import CoffeeMachine, CoffeePod
from api_v1.serializers import CoffeeMachineSerializer, CoffeePodSerializer


class CoffeeMachines(generics.ListCreateAPIView):
	"""
		get:
			Returns a list of all Coffee Machine records.
		post:
			Creates a new Coffee Machine record.
	"""

	queryset = CoffeeMachine.objects.all()
	serializer_class = CoffeeMachineSerializer
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ['product_type', 'water_line_compatible']


class CoffeeMachineDetails(generics.RetrieveUpdateDestroyAPIView):
	"""
		get:
			Returns an existing Coffee Machine Record.
		put:
			Updates an existing Coffee Machine Record.
		patch:
			Performs a partial update on an existing Coffee Machine Record.
		delete:
			Deletes a Coffee Machine Record.
	"""

	queryset = CoffeeMachine.objects.all()
	serializer_class = CoffeeMachineSerializer


class CoffeePods(generics.ListCreateAPIView):
	"""
		get:
			Returns a list of all Coffee Pods records.
		post:
			Creates a new Coffee Pod record.
	"""

	queryset = CoffeePod.objects.all()
	serializer_class = CoffeePodSerializer
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ['flavor', 'pack_size']


class CoffeePodDetails(generics.RetrieveUpdateDestroyAPIView):
	"""
		get:
			Returns an existing Coffee Pod Record.
		put:
			Updates an existing Coffee Pod Record.
		patch:
			Performs a partial update on an existing Coffee Pod Record.
		delete:
			Deletes a Coffee Pod Record.
	"""

	queryset = CoffeePod.objects.all()
	serializer_class = CoffeePodSerializer

