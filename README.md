# inovola Task
### Setup
Please install the dependencies using the following command:

```pip3 install -r requirements.txt```

### Notes
* Django is not designed to work with NoSQL Databases like MongoDB and there is no official support for them. instead, there are some open source projects for that matter and one of them is [Djongo](https://github.com/nesdis/djongo).
* When i tried to use the NoSQL models implemented in Djongo (such as, EmbeddedField... Etc.), I encountered some errors and bugs. plus, the data seemed to be well-structured so, i treated it like a relational data.

### Time Spent
9 Hours.